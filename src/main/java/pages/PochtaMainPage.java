package pages;

import io.qameta.allure.Step;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;


public class PochtaMainPage extends BasePage {

    By loginLink = new By.ByCssSelector("[href=\"/api/auth/login\"]");
    By loginButton = new By.ByCssSelector("form > button");
    By myAccountMenu = new By.ByCssSelector("div[title='79917133197']");
    By myTrackingLink = new By.ByCssSelector("div > a[href=\"/tracking\"]");
    By loginField = new By.ByCssSelector("#username");
    By passwordField = new By.ByCssSelector("#userpassword");
    By forComparison = new By.ByCssSelector("div > p");
    String expectedString = "Сохраняйте отправления, чтобы узнавать по электронной почте, где они находятся";

    public PochtaMainPage(WebDriver driver) {
        this.driver = driver;
    }

    @Step("Открытие главной страницы")
    public PochtaMainPage open() {
        driver.get("https://www.pochta.ru/");
        getScreenshot();
        logger.info("Загружена главная страница");
        return this;
    }

    @Step("Нажатие ссылки \"Войти\"")
    public void clickLoginLink() {
        driver.findElement(loginLink).click();
        getScreenshot();
        logger.info("Нажата ссылка Войти");
    }

    @Step("Ввод логина")
    public void inputLogin(String userName) {
        driver.findElement(loginField).sendKeys(userName);
        getScreenshot();
        logger.info("Введён логин");
    }

    @Step("Ввод пароля")
    public void inputPassword(String userPassword) {
        driver.findElement(passwordField).sendKeys(userPassword);
        getScreenshot();
        logger.info("Введён пароль");
    }

    @Step("Нажатие кнопки \"Войти\" после ввода пароля")
    public void clickButton() {
        driver.findElement(loginButton).click();
        getScreenshot();
        logger.info("Нажата кнопка Войти");
    }

    @Step("Наведеие на аккаунт для выпадающего списка")
    public void clickAccountMenu() {
        Actions actions = new Actions(driver);  //Instantiate Action Class
        WebElement menuOption = driver.findElement(myAccountMenu);  //Retrieve WebElement to perform mouse hover
        actions.moveToElement(menuOption).perform();    //Mouse hover menuOption
        getScreenshot();
        logger.info("Отображен список при наведении");
    }

    @Step("Нажатие \"Мои отправления\"")
    public void clickTracking() {
        driver.findElement(myTrackingLink).click();
        getScreenshot();
        logger.info("Открыта страница отправлений");
    }

    @Step("Сравнение данных")
    public void compareData() {
        Assertions.assertEquals(expectedString, driver.findElement(forComparison).getText());
        getScreenshot();
        logger.info("Сравнение проведено");
    }

}
