import io.qameta.allure.*;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import pages.PochtaMainPage;


@DisplayName("Тест на соответствие содержимого страницы поиска")
public class PochtaTest extends BaseTest {
    @Test
    @Story("Авторизация на сайте pochta.ru, переход в отправления, проверка текста")
    @Epic("Эпик")
    @Description("Проверка текста")
    @Severity(SeverityLevel.BLOCKER)
    @DisplayName("ДЗ 4 Практика с тестированием UI")

    public void compareTest() {
        PochtaMainPage pochtaMainPage = new PochtaMainPage(driver);
        pochtaMainPage.open();
        pochtaMainPage.clickLoginLink();
        pochtaMainPage.inputLogin("tojoxi2718@mahazai.com");
        pochtaMainPage.inputPassword("1QAZ2wsx");
        pochtaMainPage.clickButton();
        pochtaMainPage.clickAccountMenu();
        pochtaMainPage.clickTracking();
        pochtaMainPage.compareData();
    }
}
